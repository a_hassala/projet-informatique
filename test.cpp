#include <iostream>
#include "./headers/Display.hpp"
#include "./headers/window.hpp"
#include <gtkmm-2.4/gtkmm/main.h>
#include <unistd.h>


int main(int argc, char **argv) {

	if(!detectAndDisplayImage("./Images/lena.png"))
		std::cout<<"Cascade et Image  de face chargées et traitées avec succèes\n\n" <<std::endl;
	else exit(-1);

	sleep(5);

	if(!detectAndDisplayImage("./Images/profile.jpg"))
		std::cout<<"Cascade et Image  de profile chargées et traitées avec succèes\n\n" <<std::endl;
	else exit(-1);

	sleep(5);

	if(!DetectAndSave("./Videos/bitchy.mp4"))
		std::cout<<"Cascade et Vidéo  chargées,traitées et enregistrées avec succèes\n\n" <<std::endl;
	else exit(-1);


	return 0;

}