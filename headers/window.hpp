#ifndef GTKMM_MYWINDOW_H
#define GTKMM_MYWINDOW_H

#include <gtkmm-2.4/gtkmm.h>


/*	Cette fonction permet de définir une fenêtre 
	Les caractéristiques de cette fenêtre sont ensuite définies dans "window.cpp".
	On inclue alors tous les widgets qui vont apparaître dans la fenêtre initiale :
	du simple bouton aux widgets plus complexes comme le Filechooser Dialog ou le
	Notebook.
	Enfin, on fait des inclusions des widgets appropriés dans les widgets containers 
	qui leur correspondent. Et on définit un événement qui permettra de lancer la 
	fonction voulue (exemple : clique de souris sur un bouton). */


class MyWindow : public Gtk::Window

{

public:
 	MyWindow();
  	virtual ~MyWindow();

  	/*	Cette fonction permet de définir une fenêtre 
	Les caractéristiques de cette fenêtre sont ensuite définies dans "window.cpp".
	On inclue alors tous les widgets qui vont apparaître dans la fenêtre initiale :
	du simple bouton aux widgets plus complexes comme le Filechooser Dialog ou le
	Notebook.
	Enfin, on fait des inclusions des widgets appropriés dans les widgets containers 
	qui leur correspondent. Et on définit un événement qui permettra de lancer la 
	fonction voulue (exemple : clique de souris sur un bouton). */

protected:
 
  //Signal handlers:
  
  	void on_button_file_clicked();

  	/* Fonction qui permet de lancer une fenêtre de sélection de fichiers lorsque 
  	l'événement "clique sur le bouton "choisir une image" ou "choisir une vidéo"" 
  	est réceptionné.*/

  	void on_button_quit();

  	/* Fonction qui permet de fermer la fenêtre principale lors de la réception 
  	de l'événement "clique sur le bouton "Quitter"".*/

	void on_notebook_switch_page(GtkNotebookPage* page, guint page_num);

	/* Fonction qui permet de changer d'onglet lorsque que l'événement "clique de la
	souris sur un autre onglet" est réceptionné. */


  //Child widgets:
	
	Gtk::VBox m_VBox, m_VBox1, m_VBox2, m_VBox3, m_VBox4;
	// Définition des widgets container m_VBox, m_VBox1, m_VBox2, m_VBox3 et m_VBox4
	Gtk::HBox m_HBox;
	// Définition du widget container m_HBox
 	Gtk::Notebook m_Notebook;
 	// Définition du widget container m_Notebook
 	Gtk::Label m_Label;
 	// Définition du widget m_Label
  	Gtk::VButtonBox m_ButtonBox, m_ButtonBox2, m_ButtonBox3, m_ButtonBox4, m_ButtonBox5, m_ButtonBox6, m_ButtonBox7;
  	// Définition des widgets container m_ButtonBox, m_ButtonBox2, m_ButtonBox3, m_ButtonBox4, m_ButtonBox5, m_ButtonBox6 et m_ButtonBox7
  	Gtk::Button m_Button_Quit, m_Button_Play, m_Button_File, m_Button_Open, m_Button_File2, m_Button_Open2;
  	// Déniftion des widgets m_Button_Quit, m_Button_Play, m_Button_File, m_Button_Open, m_Button_File2 et m_Button_Open2

#endif //GTKMM_MYWINDOW_H
} ;