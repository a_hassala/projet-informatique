

/**
 * \file Display.hpp
 * \brief Signature des Fonctions utilisée
 *
 * Module qui gère l'affichage d'une image ou d'une vidéo
 *
 */

#ifndef DISPLAY_HPP_
#define DISPLAY_HPP_
#include <opencv2/opencv.hpp>

	/**
	 * \fn int detectAndDisplayImage (std::string)
	 * \brief Detecte les visages
	 *
	 * Il s'agit de la fonction principale du programme
	 * Elle prends comme parametre une frame, un double correpondant a l'echelle ( un facteur de précision de la detection et un int pour afficher ou non le resultat)
	 * On y déclare plusieurs varible comme les faces et profiles qui sont 2 vecteurs rectangulaires qui représenterons les visages détectés
	 *
	 * On commence par transformer la frame d'argument en une frame en noir et blanc ce qui augmente la précision de la detection
	 *
	 * on charge ensuite les cascades de face et de profile et on vérifie qu'elles sont bien ouvertes
	 *
	 * La detection de visage sur l'image peut donc commencer
	 * on lance donc DectectMultiScale avec comme paramètre la frame en noir et blanc, le vecteur de visage faces (ou profiles), le Double pour l'echelle, le nombre de voisin est 2, 0 pour le flags, on prend toutes les tailles possibles.
	 *
	 * on crée une ellipse pour les visages de face et un rectancle pour les visages de profile
	 *
	 * On affiche ou non le resultat dans la fenetre crée.
	 *
	 */

int detectAndDisplayImage (std::string);


int DetectAndSave(std::string) ;

	/*
	 * Cette fonction a pour but de detecter les visages sur une vidéo et de sauvegarder en un fichier nommé Face_Detect_on_Video.avi le résultat
	 * On commence par ouvrir la vidéo suivant l'argument qui est le chemin de celle-ci pour pouvoir la lire
	 * On définie ensuite quelques variables comme la frame et la fenêtre d'affichage et le Scale (un des paramètre de notre detection)
	 * Puis on test que la vidéo s'est bien ouverte sinon on affiche un message d'erreur et on arrete la fonction
	 * Si tout se passe bien un récupère quelques informations sur la vidéo qu'on traite comme le nombre de Frames total, et les dimensions de la vidéo
	 *
	 * On charge maintenant la classe VideoWriter pour créer notre fichier résultat
	 *
	 * on lance donc la detection frame par frame en les parcourants tous
	 * on vérifie que la frame n'est pas vide( ce qui sera aussi notre marqueur de fin de boucle)
	 * Et on lance la fonction detectAndDisplay avec comme paramètre courant la frame actuelle, l'échelle(Scale) et le paramètre 0 pour ne pas affcher le resulat
	 * enfin on récupere ne numéro de la frame actuelle pour afficher un avancement de la fonction
	 * sans oublier de rajouter la frame actuelle à notre vidéo résultat.
	 *
	 */

void LiveDetect(  );
	/*
	 * Detection de visage appartir de la vidéo de la cam
	 * On définit d'abord les différentes variable, 1 pour la vidéo de capture, une pour la frame et enfin un paramètre pour la detection de visages
	 * --1
	 * Lance la CAM et récupère le stream de la vidéo
	 * Puis on test que la cam est bien lancé
	 * la boucle While permet de travailler Image par Image tant que la vidéo est lue
	 * on fait un rapide test pour être sur de toujours avoir une image sur laquelle travailler
	 * --2
	 * On peut donc lancer la dectection sur l'image, avec le facteur d'échelle
	 * --3
	 * on définit une touche de fermeture ici echap qui permettra de détruire la fenêtre ouverte.
	*/



#endif /* DISPLAY_HPP_ */
