/*
 * DetectAndDisplay.cpp
 *
 *  Created on: Mar 24, 2014
 *      Author: marlow
 */


#include <opencv2/opencv.hpp>
#include <iostream>
#include "../headers/Display.hpp"
#include <stdio.h>
#include <unistd.h>

using namespace cv;
using namespace std;

string profile_cascade_name = "./data/haarcascades/haarcascade_profileface.xml";
string face_cascade_name = "./data/haarcascades/haarcascade_frontalface_alt.xml";
CascadeClassifier face_cascade;
CascadeClassifier profile_cascade;

void detectAndDisplay( Mat frame, double ScaleFactor, int display )
  {
	String window_name = "Capture - Face detection";
	vector<Rect> faces;
	vector<Rect> profiles;
	Mat frame_gray;

	cvtColor( frame, frame_gray, COLOR_BGR2GRAY );
	equalizeHist( frame_gray, frame_gray );

	//-- Chargement des Cascades de face
	if( !face_cascade.load( face_cascade_name ) ){
		cout << ("--(!)Erreur de chargement de la cascade de face\n");
		}

	//--Chargement des cascades de profile
	if( !profile_cascade.load( profile_cascade_name ) ){
		cout << ("--(!)Erreur de chargement de la cascade de profile\n");
		}

	//-- Detection du visage et des profiles

			face_cascade.detectMultiScale( frame_gray, faces, ScaleFactor,2, 0|CASCADE_SCALE_IMAGE,Size(),Size() );
			profile_cascade.detectMultiScale( frame_gray, profiles, ScaleFactor,2, 0|CASCADE_SCALE_IMAGE,Size(),Size() );

	//-- Placement du Marqueur pour les Visages de face
			for ( size_t i = 0; i < faces.size(); i++ )
			{
				Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
				ellipse( frame, center, Size( faces[i].width/2, faces[i].height/2 ), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
			}

			for ( size_t j = 0; j < profiles.size(); j++ )
			{
				rectangle(frame, profiles[j], CV_RGB(0, 255,0), 1);
			}


	//-- Affiche le résultat
		if (display==1)
			imshow( window_name, frame );

}

int detectAndDisplayImage(string img_path){
	Mat frame ;
	double ScaleFactor = 1.1; // facteur de reduction de la taille d'image ;
	frame = imread(img_path, -1 );

	if (frame.empty()){
    	cout << "Probleme de chargement de l'image" + img_path << endl;
    	return -1;
	}

    detectAndDisplay(frame, ScaleFactor,1);

	return 0;

}

void LiveDetect( void )
{
    VideoCapture capture;
    Mat frame;
    double ScaleFactor = 2; // facteur de reduction de la taille d'image ;

    //-- 1. Lecture du Stream
    capture.open( -1 );
    if ( ! capture.isOpened() ) { cout << "--(!)Error opening video capture\n"; //return -1;
     }

    cout << "Appuyer sur echap pour quitter\n";

    while ( capture.read(frame) )
    {
        if( frame.empty() )
        {
            cout << " --(!) No captured frame -- Break!";
            break;
        }
        //-- 2. Utilisation du classifier pour Detecter le visage
          detectAndDisplay( frame , ScaleFactor,1) ;


        //-- 3. Attente de la fermeture
            int c=waitKey(10);
          if( (char)c == 27 ) { break; } // escape
      }
      //return 0;
  }



int DetectAndSave(string filename	)
{
    VideoCapture capture(filename);
    Mat frame;
    namedWindow( "w", 1);
    double Scale = 1.5;

    if( !capture.isOpened() )
    {
    	cout << "erreur lors du chargement de la vidéo" << endl ;
    	return -1 ;
    }

	int nbframes = (int) capture.get(CV_CAP_PROP_FRAME_COUNT);
	int tmpw = (int) capture.get(CV_CAP_PROP_FRAME_WIDTH);
	int tmph = (int) capture.get(CV_CAP_PROP_FRAME_HEIGHT);

	VideoWriter writer("Face_Detect_on_Video.avi", CV_FOURCC('M','J','P','G'), 25, cvSize( tmpw, tmph ));




    for( ; ; )
    {
        capture >> frame;
        if(frame.empty())
            break;
        int current_pos=(int)capture.get(CV_CAP_PROP_POS_FRAMES);
        if (!(current_pos%10))
        	detectAndDisplay(frame,Scale,0);
        cout << "traitement en cours: " << current_pos <<" / " << nbframes << endl ;
        writer << frame ;

    }
    return 0;
}




