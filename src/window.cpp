#include <iostream>
#include "../headers/window.hpp"
#include "../headers/Display.hpp"
#include <unistd.h>



std::string filename ;

void image_click(){
	
	detectAndDisplayImage(filename);

}

void video_click(){
	DetectAndSave(filename);
}

MyWindow::MyWindow()

:	m_Button_Play ("Lancer la Livecam"), //caractérisation du bouton m_Button_Play
	m_Button_File("Choisir une vidéo"), //caractérisation du bouton m_Button_File
	m_Button_Open("Traiter la vidéo sélectionnée"), //caractérisation du bouton m_Button_Open
	m_Button_File2("Choisir une image"), //caractérisation du bouton m_Button_File2
	m_Button_Open2("Ouvrir l'image traitée"), //caractérisation du bouton m_Button_Open2
	m_Button_Quit("Quitter") //caractérisation du bouton m_Button_Quit
{

// Dimensionnement de la fenêtre principale

	set_title("Adaboost"); //définition du titre de la fenêtre principale
	set_border_width(10); //dénifition de la largeur des bordures
	set_default_size(800, 400); //dimensionnement de la fenêtre principale
	add(m_VBox); //ajout du widget container "m_VBox" à la fenêtre principale

//Positionnement des onglets en haut de la fenêtre principale

	m_Notebook.set_border_width(10); //définition de la largeur des bordures
	m_VBox.pack_start(m_Notebook); 
	//ajout du widget container "m_Notebook" au widget "m_VBox"
	m_VBox.pack_start(m_ButtonBox, Gtk::PACK_SHRINK);
	//ajout du widget container "m_ButtonBox" au widget "m_VBox"
	m_ButtonBox.pack_start(m_Button_Quit, Gtk::PACK_SHRINK);
	//ajout du widget "m_Button_Quit" au widget "m_ButtonBox" qui définit alors le bouton "Quitter"
	m_Button_Quit.signal_clicked().connect(sigc::mem_fun(*this, &MyWindow::on_button_quit) );
	/*permet de quitter la fenêtre lorsque l'événement "clique sur le bouton "Quitter"
	est réceptionné*/


//Ajout du nombre de pages nécessaires

	m_Notebook.append_page(m_Label, "Bienvenue");
	//ajout de l'onglet "Bienvenue"
	m_Notebook.append_page(m_VBox2, "Livecam");
	//ajout de l'onglet "Livecam"
	m_Notebook.append_page(m_VBox3, "Vidéo");
	//ajout de l'onglet "Vidéo"
	m_Notebook.append_page(m_VBox4, "Image");
	//ajout de l'onglet "Image"
	m_Notebook.signal_switch_page().connect(sigc::mem_fun(*this, &MyWindow::on_notebook_switch_page) );
	//permet de changer d'onglet lorsque l'événement "clique sur un autre onglet" est réceptionné


//Spécification du contenu de l'onglet "Welcome"

	m_Label.set_text("Bienvenue dans le programme de détection de visages avec Adaboost." 
					"\nVous pouvez désormais choisir de lancer la détection sur le support de votre choix : "
					"			\n- une vidéo"
					"			\n- une image" 
					"			\n- directement sur la Livecam"
					"\nDes onglets spécifiques ont été mis en place à cet effet."
					"\n\nLes auteurs : HASSALA Anasse, PIERRE Marlène, REMBERT "
					"Gabrielle et TALEB Elias");


//Spécification du contenu de l'onglet "Livecam"

	m_VBox2.pack_start(m_ButtonBox2);
	//ajout du widget container "m_ButtonBox2" au widget "m_VBox2"
	m_ButtonBox2.pack_start(m_Button_Play);
	//ajout du widget "m_Button_Play" au widget "m_ButtonBox2" qui définit alors le bouton "Lancer la Livecam"
	m_Button_Play.signal_clicked().connect(sigc::ptr_fun(&LiveDetect));
	/*permet de lancer la Livecam lorsque l'événement "clique sur le bouton "Lancer la Livecam"
	est réceptionné*/


//Spécification du contenu de l'onglet "Video"	

	m_VBox3.pack_start(m_ButtonBox4);
	//ajout du widget container "m_ButtonBox4" au widget "m_VBox3"
	m_VBox3.pack_start(m_ButtonBox5);
	//ajout du widget container "m_ButtonBox5" au widget "m_VBox3"
	m_ButtonBox4.pack_start(m_Button_File);
	//ajout du widget "m_Button_File" au widget "m_ButtonBox4" qui définit alors le bouton "Choisir une vidéo"
	m_ButtonBox5.pack_start(m_Button_Open);
	//ajout du widget "m_Button_Open" au widget "m_ButtonBox5" qui définit alors le bouton "Traiter la vidéo sélectionnée"
	m_Button_File.signal_clicked().connect(sigc::mem_fun(*this, &MyWindow::on_button_file_clicked) );
	/*permet de choisir le fichier vidéo lorsque l'événement "clique sur le bouton "Choisir une vidéo"
	est réceptionné*/
	m_Button_Open.signal_clicked().connect(sigc::ptr_fun(&video_click));
	/*permet de traiter le fichier vidéo sélectionné lorsque l'événement "clique sur le bouton "Traiter la vidéo sélectionné"
	est réceptionné en faisant appel à la fonction "video_click"*/


//Spcification du contenu de l'ongle "Picture"

 	m_VBox4.pack_start(m_ButtonBox6);
 	//ajout du widget container "m_ButtonBox6" au widget "m_VBox4"
 	m_VBox4.pack_start(m_ButtonBox7);
 	//ajout du widget container "m_ButtonBox7" au widget "m_VBox4"
 	m_ButtonBox6.pack_start(m_Button_File2);
 	//ajout du widget "m_Button_File2" au widget "m_ButtonBox6" qui définit alors le bouton "Choisir une image"
 	m_ButtonBox7.pack_start(m_Button_Open2);
 	//ajout du widget "m_Button_Open2" au widget "m_ButtonBox7" qui définit alors le bouton "Ouvrir l'image traitée"
 	m_Button_File2.signal_clicked().connect(sigc::mem_fun(*this, &MyWindow::on_button_file_clicked) );
	/*permet de choisir le fichier image lorsque l'événement "clique sur le bouton "Choisir une image"
	est réceptionné*/
	m_Button_Open2.signal_clicked().connect(sigc::ptr_fun(&image_click));
	/*permet d'ouvrir le fichier image sélectionné aprés traitement lorsque l'événement 
	"clique sur le bouton "Traiter la vidéo sélectionné" est réceptionné en faisant 
	appel à la fonction "image_click"*/


	show_all_children();
}

MyWindow::~MyWindow()
{}

//définition de la classe virtuelle "MyWindow"

void MyWindow::on_button_file_clicked()
{
  	Gtk::FileChooserDialog dialog("Please choose a file", Gtk::FILE_CHOOSER_ACTION_OPEN);
  	dialog.set_transient_for(*this);

  	//Add response buttons the the dialog:
  	dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  	dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);


	Gtk::FileFilter filter_any;
  	filter_any.set_name("Any files");
  	filter_any.add_pattern("*");
  	dialog.add_filter(filter_any);

  	//Show the dialog and wait for a user response:
  	int result = dialog.run();

  	//Handle the response:
  	switch(result)
  	{
    	case(Gtk::RESPONSE_OK):
    	{
     	 	std::cout << "Open clicked." << std::endl;

      		//Notice that this is a std::string, not a Glib::ustring.
      		filename = dialog.get_filename();
      		std::cout << "File selected: " <<  filename << std::endl;
      		break;
    	}
    	case(Gtk::RESPONSE_CANCEL):
    	{
      		std::cout << "Cancel clicked." << std::endl;
      		break;
    	}
    	default:
    	{
      		std::cout << "Unexpected button clicked." << std::endl;
      	break;
    	}
  	}
}


void MyWindow::on_button_quit()
{
	hide();
}

void MyWindow::on_notebook_switch_page(GtkNotebookPage* /* page */, guint page_num)
{
  std::cout << "Switched to tab with index " << page_num << std::endl;

}
