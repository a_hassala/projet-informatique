OBJS := main.o window.o DetectAndDisplay.o



#Cible Principale
ALL: face_detect

face_detect:$(OBJS)
	@echo " "
	@echo "Construction de l'executable"
	@echo "Édition de lien : ld"
	g++ -Wall $(OBJS) -L/usr/lib/x86_64-linux-gnu/ -o face_detect `pkg-config gtkmm-2.4 --cflags --libs` `pkg-config opencv --cflags --libs`
	@echo " "
	@echo "Construction de l'executable de tests unitaires"
	g++ -g -Wall -I./headers/ `pkg-config gtkmm-2.4 --cflags` `pkg-config opencv --cflags ` -c test.cpp
	g++ -Wall test.o ./DetectAndDisplay.o -L/usr/lib/x86_64-linux-gnu/ -o test `pkg-config gtkmm-2.4 --cflags --libs` `pkg-config opencv --cflags --libs` 
	@echo " "
	@echo "Suppresion des objects"
	rm *.o
	@echo " "
	@echo "=====Executables construit avec Succes======"

#Objets

main.o: ./src/main.cpp
	@echo " "
	@echo "Contruction de main.o"
	@echo "Compilation : g++"
	g++ -g -Wall -I./headers/ `pkg-config gtkmm-2.4 --cflags` -c ./src/main.cpp

DetectAndDisplay.o: ./src/DetectAndDisplay.cpp	
	@echo " "
	@echo "Contruction de DetecAndDisplay.o"
	@echo "Compilation : g++"
	g++ -g -Wall -I./headers/ `pkg-config opencv --cflags ` -c ./src/DetectAndDisplay.cpp

window.o: ./src/window.cpp
	@echo " "
	@echo "Contruction de window.o"
	@echo "Compilation : g++"
	g++ -g -I./headers/ -c `pkg-config gtkmm-2.4 --cflags ` ./src/window.cpp


clean: 
	@echo "Suppression de l'executable,des fichiers objets et de la video traitée"
	rm -rf face_detect test *.o Face_Detect_on_Video.avi
